# PESTERI in Docker container
# 1. Prepare old ubuntu image :
# 	git clone https://github.com/ds82/ubuntu-hardy-8.04-i386-docker.git
# 	./build.sh
# 2. Copy htdocs from the disk to the current folder
# 3. Build :
# 	docker build -t pesteri .
# 4. Run:
# 	docker run -i -p 80:80 -p 8888:80 -t pesteri
# 5. Available in browser at http://localhost

FROM ubuntu:8.04

RUN apt-get update
RUN apt-get install -y lighttpd php5 php5-cgi sqlite nano php5-sqlite
RUN lighty-enable-mod userdir fastcgi fastcgi-php php5-cgi

#Either copy in the container or mount as a volume
#COPY htdocs/ /var/www/

EXPOSE 80

CMD ["lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]