# README #

PESTERI (http://iskar-speleo.org/drupal/?q=PESTERI) as a Docker container (https://hub.docker.com/r/lz1asl/pesteri/)

### Prebuild container ###

1. Navigate next to the htdocs folder
1. Run: ```docker run -i -p 80:80 -p 8888:80 -v `pwd`/htdocs/:/var/www/ lz1asl/pesteri```
2. Available in browser at http://localhost

### Local build ###

1. Prepare old ubuntu image :
	```git clone https://github.com/ds82/ubuntu-hardy-8.04-i386-docker.git
	./build.sh```
2. Copy htdocs from the disk to the current folder
3. Build:
	```docker build -t pesteri .```
4. Run:
	```docker run -i -p 80:80 -p 8888:80 -t pesteri```
5. Available in browser at http://localhost